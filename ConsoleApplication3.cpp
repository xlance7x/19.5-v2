﻿#include <iostream>
using namespace std;

class animal
{
public:
	virtual void Voice() const = 0;
};

class Dog : public animal
{
public:
	void Voice() const override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public animal
{
public:
	void Voice() const override
	{
		cout << "Meow!" << endl;
	}
};

class Mouse : public animal
{
public:
	void Voice() const override
	{
		cout << "Squeak!" << endl;
	}
};

int main()
{
	const int Size = 3;
	animal* animals[Size] = { new Dog, new Cat, new Mouse };

	for (animal* a : animals)
		a->Voice();
}

